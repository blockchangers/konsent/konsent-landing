const HDWalletProvider = require("truffle-hdwallet-provider")
const infura_apikey = "TIkfho43TLfovnkTJhiC"
const mnemonic = "glare modify fine lazy practice flee equip foam message insect camp approve"

module.exports = {
    migrations_directory: "./migrations",
    networks: {
        development: {
            host: "localhost",
            port: 8545,
            network_id: "*", // Match any network id // 19071 / *
            gas: 4712388
        },
        blockchangers: {
            provider: function() {
                return new HDWalletProvider(mnemonic, 'https://blockchangersnode.foyn.co ') // https://blockchangersnode.foyn.co/    'http://46.101.230.136:8501'
            },
            network_id: 28232,
            gas: 7494037,
            gasPrice: 400000000000
        },
        rinkeby: {
            provider: function() {
                return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/" + infura_apikey)
            },
            network_id: 4,
            gas: 7494037,
            gasPrice: 400000000000
        },
        ropsten: {
            provider: function() {
                return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/aJzbES9Dohx46EYrE4vg")
            },
            network_id: 3
        },
        kovan: {
            provider: function() {
                return new HDWalletProvider(mnemonic, "https://kovan.infura.io/aJzbES9Dohx46EYrE4vg")
            },
            gas: 7900000,
            gasPrice: 100000000000 * 2,
            network_id: 42
        },
        gan: {
            provider: function() {
                return new HDWalletProvider("robot property middle brass panic aspect settle zero virus mask album marble", 'HTTP://127.0.0.1:7545')
            },
            network_id: 5777,
            gas: 6721975,
            gasPrice: 20000000000
        }
    }
};
