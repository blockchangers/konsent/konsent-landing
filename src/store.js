import Vue from 'vue'
import Vuex from 'vuex'

import magicCard from './components/magicCard/MagicCardStore'

import web3 from './components/web3/web3Store'
import wallet from './components/web3/walletStore'
import magicCardContract from './components/web3/magicCardContractStore'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        // web3, wallet,  magicCardContract, magicCard
    },
    strict: debug
})
