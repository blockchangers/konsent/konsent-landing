import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import BootstrapVue from 'bootstrap-vue'


import Icon from 'vue-awesome/components/Icon'

import 'vue-awesome/icons/gavel'
import 'vue-awesome/icons/hand-holding-heart'
import 'vue-awesome/icons/eye-slash'

Vue.component('icon', Icon)

Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

// fix for what seem to be a node 10.10.0 problem
if(process.versions === undefined){
    process.versions = {}
    process.versions.node = "10.10.0"
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
