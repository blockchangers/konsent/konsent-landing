import Vue from 'vue'
import magicCardModel from './MagicCardModel'

// initial state
const state = {
  magicCardList: [],
  magicCards: {},
  selected: magicCardModel
}

// getters
const getters = {
  magicCardList: state => state.magicCardList,
  magicCards: state => state.magicCards,
  selected: state => state.selected,
}

// actions
const actions = {
  createMagicCard({dispatch}, payload) {
    return new Promise(async (resolve, reject) => {

    })
  },
  getMagicCards({dispatch, commit}, payload) {
    return new Promise(async (resolve) => {
      let data = await dispatch('magicCardContract/getMagicCards', {}, {root: true})
      commit('saveMagicCardToState', data)
      resolve(data)
    })
  },
}
// mutations
const mutations = {
  saveMagicCardToState(state, data) {
    Vue.set(state, 'magicCards', data)
  },
  saveMagicCardListToState(state, data) {
    Vue.set(state, 'magicCardList', data)
  },
}

const namespaced = true
export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
