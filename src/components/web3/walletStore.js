const lightwallet = require('eth-lightwallet')
import Web3 from 'web3'
import Web3Config from './web3Config'

let web3 = window.web3
let web3Test = true

if (!web3Test && typeof web3 !== 'undefined') {
  web3 = new Web3(web3.currentProvider);
} else {
  web3 = new Web3(new Web3.providers.HttpProvider(Web3Config.URL));
}

// Shall module be namespacedw
const namespaced = true

// initial state
const state = {
    wallets: {},
    walletList: [],
    walletSelected: null,
    walletFlow: {
        password: null,
        seed: null,
        seedConfirmed: null
    }
}

// getters
const getters = {
    wallets: state => state.wallets,
    walletList: state => state.walletList,
    walletSelected: state => state.walletSelected,
    walletFlow: state => state.walletFlow,
    walletFlowPassword: state => state.walletFlow.password,
    walletFlowSeed: state => state.walletFlow.seed,
    walletFlowSeedConfirmed: state => state.walletFlow.seedConfirmed,
    currentWallet: state => state.wallets[state.walletList[state.walletSelected]]
}

// actions
const actions = {
    decryptWallet({}, payload){
        // Define default data
        payload = {
            password: null,
            ...payload
        }
        console.log(payload.password)
        if ( !payload.password || 0 === payload.password.length) {
            payload.password = prompt("Password", "");
        }
        return web3.eth.accounts.decrypt(payload.wallet, payload.password)
    },
    privateKeyFromSeed: (seed, password) => {
        return new Promise((resolve, reject) => {
            lightwallet.keystore.createVault({
                    password: password,
                    seedPhrase: seed,
                    hdPathString: "m/0'/0'/0'"
                },
                (err, ks) => {
                    if (err) {
                        reject(err)
                    }
                    ks.keyFromPassword(password, (err, pwDerivedKey) => {
                        if (!ks.isDerivedKeyCorrect(pwDerivedKey)) {
                            reject(err)
                        }
                        try {
                            ks.generateNewAddress(pwDerivedKey, 1)
                            resolve(ks)
                        } catch (err) {
                            reject(err)
                        }
                    })
                })
        })
    },
    createWallet: function (seed, password) {
        return new Promise((resolve, reject) => {
            this.privateKeyFromSeed(seed, password)
                .then((ks) => {
                    ks.keyFromPassword(password, async (err, pwDerivedKey) => {
                        if (err) {
                            reject(new Error("Could not get password from KeyStore"))
                            return false
                        }
                        const privateKey = ks.exportPrivateKey("0x" + ks.addresses[0], pwDerivedKey)
                        let wallet = Web3.eth.accounts.encrypt(privateKey, password)
                        wallet.address = '0x' + ks.addresses[0]
                        resolve(wallet)
                    })
                })
        })
    },
    create({commit, dispatch}, payload) {
        return new Promise(async (resolve) => {
            dispatch('createWallet', {seed: payload.seed, password: payload.password})
                .then((wallet) => {
                    const walletId = state.walletList.length
                    commit('setWallet', {wallet: wallet, walletId: walletId})
                    commit('setWalletSelected', walletId)
                    commit('clearWalletFlow')
                    dispatch('Web3/fund', {to: wallet.address}, {root: true})
                    return walletId
                })
                .then((walletId) => {
                    dispatch('saveLocal')
                    console.log("Wallet has been saved locally")
                    resolve(walletId)
                })
        })
    },
    createSeed: () => {
        let seed = lightwallet.keystore.generateRandomSeed()
        console.log("The seed generated is", seed)
        return seed
    },
    userSetPassword({commit}, data) {
        commit('setWalletFlowPassword', data.password)
        commit('setWalletFlowSeed', {seed: data.seed, password: state.walletFlow.password})
    },
    userConfirmsSeed({commit, state, dispatch}, data) {
        return new Promise((resolve, reject) => {
            if (state.walletFlow.seed === data.seed) {
                commit('setWalletFlowSeedConfirmed', data.seed)
                dispatch('create', data)
                    .then((wallet) => {
                        resolve(wallet)
                    })
            } else {
                reject(new Error(("Seed and confirmed seed does not match.")))
            }
        })
    },
    changeWallet({commit, dispatch}, walletId) {
        // To make iteasier to test things lets make select trade update when we change wllet
        commit('setWalletSelected', walletId)
        dispatch('saveLocal')
    },
    // Run this function in beforeCreated on the top component that should use this state.
    initStateFromLocalStorage({dispatch}) {
        return new Promise((resolve) => {
            const wallet = localStorage.getItem('wallet')
            if (wallet) {
                dispatch('importLocal', JSON.parse(wallet))
                resolve(JSON.parse(wallet))
            }
        })
    },
    saveStateToLocalStorage({state}) {
        localStorage.setItem('wallet', JSON.stringify(state))
    },
    fetchStateFromLocalStorage() {
        return new Promise((resolve) => {
            const wallet = localStorage.getItem('wallet')
            if (wallet) {
                resolve(JSON.parse(wallet))
            }
        })
    },
    resetWholeState({commit}, data) {
        commit('resetWholeState', data)
    },
}

// mutations
const mutations = {
    setWalletFlowPassword(state, password) {
        state.walletFlow.password = password
    },
    setWalletFlowSeed(state, seed) {
        state.walletFlow.seed = seed
    },
    setWalletFlowSeedConfirmed(state, seed) {
        state.walletFlow.seedConfirmed = seed
    },
    setWallet(state, data) {
        state.wallets = {...state.wallets, [data.walletId]: data.wallet}
        state.walletList = [...state.walletList, data.walletId]
    },
    setWalletSelected(state, walletId) {
        state.walletSelected = walletId
    },
    resetWholeState(state, localState) {
        Object.assign(state, localState)
    },
    clearWalletFlow(state) {
        Object.keys(state.walletFlow).forEach(key => {
            state.walletFlow[key] = null
        })
    }
}

export default {
    namespaced,
    state,
    getters,
    actions,
    mutations
}
