import Web3 from 'web3'
import Web3Config from './web3Config'

import * as MagicCardJSON from './../../../build/contracts/MagicCard'

let web3 = window.web3
let web3Test = true

if (!web3Test && typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    web3 = new Web3(new Web3.providers.HttpProvider(Web3Config.URL));
}

let magicCardContractStore = new web3.eth.Contract(MagicCardJSON.abi, MagicCardJSON.networks[Web3Config.ETHEREUM_NETWORK].address)

// initial state
const state = {
}

// getters
const getters = {
}

// actions
const actions = {
    createMagicCard({dispatch}, payload){
        payload = {
            name: "",
            ...payload
        }
        return new Promise(async (resolve, reject) => {
            if(payload.name === null){
                reject("Name must be defined")
            }
            let transaction = magicCardContractStore.methods.generateCard(payload.name)
            let receipt = null
            try {
                receipt = await dispatch('web3/send', {tx: transaction}, {root: true})
            } catch (e) {
                reject(e)
            }
            resolve(receipt)
        })
    },
    /*
    * Use to get an object with elements
    * */
    getMagicCards({dispatch}, payload) {
        return new Promise(async (resolve) => {
            payload = {
                amount: 5,
                order: "latest",
                ...payload
            }
            let idList = await dispatch('getMagicCardList')
            if(payload.order === "latest"){
                idList.reverse()
            }
            idList = idList.filter((element, index) => {
                return index < payload.amount
            })
            let promiseList = []
            idList.forEach(id => {
                promiseList.push(dispatch('getMagicCard', {id: id}));
            })
            let dataArray = await Promise.all(promiseList)
            // We want to pass back a Object, so we format it likewise.
            let dataObject = {}
            dataArray.map((element) => {
                dataObject[element.id] = element
            })
            resolve(dataObject)
        })
    },
    /*
    * Get a list from smart contract
    * */
    getMagicCardList({}) {
        return new Promise(async (resolve) => {
            let list = await magicCardContractStore.methods.getCardList().call()
            list = list.map(val => parseInt(val))
            resolve(list)
        })
    },
    /*
    * Get an item from smart contract
    * */
    getMagicCard({}, payload){
        return new Promise(async (resolve, reject) => {
            if(payload.id < 0){
                reject("Id must be bigger or equal to 0")
            }
            let element = await magicCardContractStore.methods.cards(payload.id).call()
            element.id = payload.id
            resolve(element)
        })
    }
}
// mutations
const mutations = {

}

const namespaced = true
export default {
    namespaced,
    state,
    getters,
    actions,
    mutations
}
