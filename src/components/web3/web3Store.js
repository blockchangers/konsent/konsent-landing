import Web3 from 'web3'
import Web3Config from './web3Config'

let web3 = window.web3
let web3Test = true

if (!web3Test && typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    web3 = new Web3(new Web3.providers.HttpProvider(Web3Config.URL));
}

// initial state
const state = {
    loading: false,
    loadingText: []
}

// getters
const getters = {
    loading: state => state.loading,
    loadingText: state => state.loadingText
}

// actions
const actions = {
    network() {
        return new Promise(function (resolve, reject) {
            // Retrieve network ID
            web3.eth.net.getId((err, networkId) => {
                if (err) {
                    // If we can't find a networkId keep result the same and reject the promise
                    reject(new Error('Unable to retrieve network ID'))
                } else {
                    // Assign the networkId property to our result and resolve promise
                    resolve(networkId)
                }
            })
        })
    },
    coinbase() {
        return new Promise(function (resolve, reject) {
            // Retrieve coinbase
            web3.eth.getCoinbase((err, coinbase) => {
                if (err) {
                    reject(new Error('Unable to retrieve coinbase'))
                } else {
                    resolve(coinbase)
                }
            })
        })
    },
    balance({dispatch}, payload) {
        return new Promise(async function (resolve, reject) {
            // Retrieve balance either for address passed with balance or coinbase
            payload = {...{address: await dispatch('coinbase')}, ...payload}
            console.log("Checking balance for address", payload.address)
            web3.eth.getBalance(payload.address, (err, balance) => {
                if (err) {
                    reject(new Error('Unable to retrieve balance for address: ' + balance))
                } else {
                    resolve(balance)
                }
            })
        })
    },
    transactionCount({}, address) {
        return new Promise(async function (resolve, reject) {
            web3.eth.getTransactionCount(address, (err, res) => {
                if (err) {
                    reject(new Error('Unable to getTransactionCount : ' + res))
                } else {
                    resolve(res)
                }
            })
        })
    },
    block() {
        return new Promise(async function (resolve, reject) {
            web3.eth.getBlock('latest', (err, block) => {
                if (err) {
                    reject(new Error('Unable to retrieve block: ' + err))
                } else {
                    resolve(block)
                }
            })
        })
    },
    send({rootGetters, commit, dispatch, state}, payload) {
        return new Promise(async (resolve, reject) => {
            commit('toggleLoading', true)
            if(state.currentPass === null){
                let newPassword = prompt("Password", "");
                commit('changeCurrentPass', newPassword)
            }
            let nextBlock = (await dispatch('block')).number + 1
            commit('addLoadingText', "Kringkaster transaksjon mot Block" + nextBlock )
            const dWallet = await dispatch('wallet/decryptWallet' , {wallet: rootGetters['Wallet/currentWallet'], password: state.currentPass}, {root: true})
            console.log("Decrypted wallet ", dWallet)

            let nonce = await dispatch('transactionCount', dWallet.address)
            // let gasEstimated = await payload.tx.estimateGas()

            // Add default things to transaction
            let newTrans = {
                ...payload.tx, ...{
                    nonce: nonce,
                    chainId: 28232
                }
            }

            let gasEstimated
            try {
                gasEstimated = await newTrans.estimateGas()
            } catch (e) {
                console.log("Could not estimate GAS, setting it manually")
                gasEstimated = 7900000
            }

            // let gasEstimated = 5000000
            console.log("Estimated gas", gasEstimated)


            console.log("new trans is", newTrans)

            let signedTx = await web3.eth.accounts.signTransaction({
                from: dWallet.address,
                gasPrice: "20000000000",
                gas: gasEstimated.toString(),
                to: payload.tx._parent._address,
                data: payload.tx.encodeABI(),
            }, dWallet.privateKey)

            // console.log("Signed TX is ", signedTx)

            web3.eth.sendSignedTransaction(signedTx.rawTransaction)
                .on('receipt', function (receipt) {
                    commit('toggleLoading', false)
                    commit('resetLoadingText')
                    resolve(receipt)
                })
                /*.on('confirmation', function (confirmationNumber, receipt) {
                    // resolve(confirmationNumber)
                    resolve(receipt)
                })*/
                .on('error', function (error) {
                    commit('toggleLoading', false)
                    commit('resetLoadingText')
                    console.log("TX sent to Blokchain, but node returned error", error)
                    reject(error)
                })
        })
    },
    async fund({dispatch}, payload) {
        return new Promise(async (resolve, reject) => {

            const fundAddress = '0xb653E63CbeD67d6b2cFa9168E9B316098453B608'
            const fundPrivateKey = '0xb85e8277133df3105c7d77ba12589e06109876dac4278d2155f3cfaaf0850a3f'

            payload = {...{amount: 1}, ...payload}
            console.log("pay", payload)
            console.log("wei", web3.utils.toWei(String(1), 'ether'))

            let estimatedGas = await web3.eth.estimateGas({
                to: payload.to,
                value: web3.utils.toWei(String(payload.amount), 'ether')
            })

            let signedTx = await web3.eth.accounts.signTransaction({
                from: fundAddress,
                gas: estimatedGas.toString(),
                to: payload.to,
                value: web3.utils.toWei(String(payload.amount), 'ether')
            }, fundPrivateKey)

            web3.eth.sendSignedTransaction(signedTx.rawTransaction)
                .on('receipt', async function (receipt) {
                    console.log("Funding complete balance is now ", await dispatch('balance', {address: payload.to}))
                    resolve(receipt)
                })
                /*.on('confirmation', function (confirmationNumber, receipt) {
                    // resolve(confirmationNumber)
                    resolve(receipt)
                })*/
                .on('error', function (error) {
                    console.log("TX sent to Blokchain, but node returned error", error)
                    reject(error)
                })

        })
    },
}
// mutations
const mutations = {
    toggleLoading(state, boolean) {
        state.loading = boolean;
    },
    changeCurrentPass(state, newPass) {
        state.currentPass = newPass
    },
    addLoadingText(state, text) {
        state.loadingText.push(text)
    },
    removeLoadingText(state, text) {
        let index = state.loadingText.indexOf(text)
        if (index !== -1) state.loadingText.splice(index, 1);
    },
    resetLoadingText(state) {
        state.loadingText = []
    }
}

const namespaced = true
export default {
    namespaced,
    state,
    getters,
    actions,
    mutations
}
