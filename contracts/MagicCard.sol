pragma solidity ^0.4.24;

import './../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721BasicToken.sol';

contract MagicCard is ERC721BasicToken {

    struct Card {
        uint8 power;
        uint8 health;
        string name;
    }

    Card[] public cards;
    uint256[] public cardList;

    constructor(string genesisCardName) public {
        generateCard(genesisCardName);
    }

    function generateCard(
        string name
    ) public {
        Card memory _card = Card({
            power : uint8(block.timestamp) % 10,
            health : uint8(block.timestamp - block.number) % 10,
            name : name
            });

        // save property
        uint256 cardId = cards.push(_card) - 1;
        _mint(msg.sender, cardId);
        cardList.push(cardId);
    }

    // Getters

    function getCardList() public view returns (uint256[] memory) {
        return cardList;
    }

}
