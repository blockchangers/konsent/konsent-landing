/* eslint-disable-next-line */
let MagicCard = artifacts.require("./MagicCard.sol")

module.exports = function (deployer) {
    let MagicCardInstance


    deployer.deploy(MagicCard, "The Genesis Magician")
        .then(function() {
            /*
            * Chain depended contracts here
            * */
            // return deployer.deploy(SOMENEWCONTRACT, Card.address);
        })

        /*
        * After all contract are deployed, we can retrive their instances
        * */
        .then(function () {
            MagicCard.deployed()
                .then(function (instance) {
                    MagicCardInstance = instance
                })
        })

        /*
        * Then we can inititate functions on them
        * */
        .then(function () {
            return MagicCardInstance.generateCard("Magician Firestorm")
        })

}
