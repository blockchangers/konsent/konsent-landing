# Pippey - VUE & Web3 boilerplate

## Todo

- Fix wallet storage and UI. Copy from project sommer

## Development

1. git clone
```
git clone -b master https://gitlab.com/blockchangers-community/pippey.git
```
2. ```cd pippey ```
3. ```truffle develop``` (if windows, ```truffle.cmd develop``` )
4. In console write ```migrate --reset```
5. Open another command window, then run ``` npm run serve```
6. Visit http://localhost:8080

## Deploy

1. Run "npm run build". This will both build project into /dist folder and push the project to firebase if that has been set up.

## Test

-  Run your unit tests
```
npm run test:unit
```

- Run your end-to-end tests
```
npm run test:e2e
```

## VUE conventions

As of now we want to strictly follow https://vuejs.org/v2/style-guide/

### Cases
- PascalCase - Components
- camelCase - Stores, functions
- kebab-case -
- snake_case -

### Components
- Component names should always be multi-word, except for root “App” components.
- Each component should be in its own file.
- Filenames of single-file components should always PascalCase. Use “UserCard.vue” or "ProjectListDetails.vue". In HTML template we try to use PascalCase with self encloseing <UserDetails/> Reason: https://vuejs.org/v2/style-guide/#Component-name-casing-in-templates-strongly-recommended
- Child components should include their parent name as a prefix. For example if you would like a “Photo” component used in the “UserCard” you will name it “UserCardPhoto”. It’s for better readability since files in a folder are usually order alphabetically.
- Always use full name instead of abbreviation in the name of your components. For example don’t use “UDSettings”, use instead “UserDashboardSettings”.
- Props should always be typed.

### VUEX

- Always use camelCase when defining store modules.
- Avoid useing mapGetters and mapActions, only when needed directly in template. Use this.$store.dispatch/commit
- Remember Getters are synchronous
- Use models

### Router

- Avoid useing router if not specifically needed as a web page that can be directly routet too.

### Web3
- Use a Promise for everything
- Use models for every data point.

## VUE Structure

### /views

Should only be used for entry level view components. No business logic, only makeing sure data is available for child components.

### /components

Everything created under components should have its own feature folder.
