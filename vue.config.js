module.exports = {
  chainWebpack: config => {
    config.module
      .rule('truffle-solidity')
      .test(/\.sol/)
      .use('truffle-solidity')
      .loader('truffle-solidity')
  },
  transpileDependencies: [
    /\bvue-awesome\b/
  ]
}
